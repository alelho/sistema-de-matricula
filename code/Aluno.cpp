#include <iostream>
#include <string>
#include <iomanip>
#include <list>
#include "Aluno.h"

using namespace std;

Aluno::Aluno(string nome, const Arquivo& arqAluno)
{
    alunoNome = nome;
    arquivoAluno = arqAluno;
}

void Aluno::setNome(string nome)
{
    alunoNome = nome;
}

string Aluno::getAlunoNome()
{
    return alunoNome;
}

void Aluno::exibirHorarioDoAluno()
{
    string cod, cred, tur;
    string arq("disciplinas.csv");
    list<string> listaHora;

// Traz os dados do .hor do aluno para memoria
    arquivoAluno.DadosParaMemoria(&hor);

// verifico se veio algum dado do aluno para o vector hor
    if(hor.size() == 0)
        cout << "Nenhum Horario para visualizar!" << endl;
    else
    {
        cout << endl << "Horario de suas disciplinas:" << endl << endl;
// Percorro o vector para pegar o obj horario do aluno e com o metodo get retorno
// o codigo o credito e a turma, depois eu reutilizo o metodo pesquisarDisciplinas
// para exibir o nome da disciplina do aluno.
        for(vector<Horario>::iterator it = hor.begin(); it != hor.end(); it++)
        {
            Horario hor2 = *it;
            hor2.get(&cod, &cred, &tur);
            arquivoAluno.PesquisarDisciplinas(arq, cod.substr(0, 5), &cred);
        }

// Depois de exibir os nomes das disciplinas do aluno, ent�o busco o hor�rio da disciplina
// que � colocada na listaHora que � uma list. A hora � concatenada com o cod. e turma.
        for(vector<Horario>::iterator it = hor.begin(); it != hor.end(); it++)
        {
            Horario hor2 = *it;
            hor2.get(&cod, &cred, &tur);
            string hora = arquivoAluno.BuscaHora(cod.substr(0,5), tur);
            string aux = hora.substr(0, 3);
            listaHora.push_back((aux+"-"+cod.substr(0, 5)+"-"+tur));
            if(hora.length() > 3)
            {
                string aux2 = hora.substr(3, 3);
                listaHora.push_back((aux2+"-"+cod.substr(0, 5)+"-"+tur));
            }
            if(hora.length() > 6)
            {
                string aux2 = hora.substr(6, 3);
                listaHora.push_back((aux2+"-"+cod.substr(0, 5)+"-"+tur));
            }
        }

        int cont = 0;
        listaHora.sort(); // Ordenar lista

// Exibe a lista ordenada na horizontal, tentei fazer igual a da pucrs mas era dif�cil
// e eu n�o podia perder tempo hehe
cout << endl << "--------------------------------------------------------------------------------" << endl;
        for(list<string>::iterator it = listaHora.begin(); it != listaHora.end(); it++)
        {
            cout << setw(20) << *it;
            cont++;
            if(cont % 3 == 0)
            {
                cont = 0;
                cout << endl;
            }
        }
cout << endl << "--------------------------------------------------------------------------------" << endl;
    }
}

void Aluno::retirarAlunoDeUmaDisciplina(string cod)
{
    bool confirmaRetirada = false;
    arquivoAluno.DadosParaMemoria(&hor);

    // Quando hor2.getCodigo() retorna o codigo ele vem com o ;, ent�o o codigo que
    // o usuario fornece esta sem, ent�o tive que concatenar para o if dar verdadeiro quando iguais
    cod += ";";
    for(vector<Horario>::iterator it = hor.begin(); it != hor.end(); it++)
    {
        Horario hor2 = *it;
        if(hor2.getCodigo() == cod)
        {
            hor.erase(it);
            confirmaRetirada = true;
            break;
        }
    }

// Metodo que grava o .hor do aluno
    arquivoAluno.GravarHorarioAluno(&hor);
    if(confirmaRetirada == true)
        cout << "Disciplina cancelada." << endl;
    else
        cout << "Nao foi possivel cancelar sua disciplina." << endl;
}

void Aluno::matricularAlunoEmUmaDisciplina(string cod, string tur, string cred)
{
    string n1, n2, n3;
    arquivoAluno.DadosParaMemoria(&hor);
// VerificaDadosValidos verifica se existe a turma na disciplina que o aluno escolheu
    if(arquivoAluno.VerificaDadosValidos(cod, tur) == true)
    {
        if (arquivoAluno.VerificaConflitoHorario(&hor, cod, tur) == false)
        {
            Horario hour(cod, cred, tur);// cria um obj horario
            hor.push_back(hour);
            arquivoAluno.GravarHorarioAluno(&hor);
        }
        else
            cout << "Impossivel realizar a matricula, horario ja cadastrado" << endl;
    }
    else
        cout << "Matricula nao realizada, dados invalidos" << endl;
}

void Aluno::telaInicialDoAluno()
{
    cout << "Aluno: " << alunoNome << endl;
    cout << "Digite: 1 - Matricular em uma disciplina"
         << "\t 2 - Cancelar uma disciplina" << endl;
    cout << "\t0 - Logout" << endl;
}

Arquivo& Aluno::getArq()
{
    return arquivoAluno;
}

