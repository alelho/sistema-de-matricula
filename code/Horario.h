#ifndef HORARIO_H
#define HORARIO_H

#include <string>

using namespace std;

class Horario{
string codigo, credito, turma;

public:
    Horario(string cod, string cred, string tur);
    void set(string cod, string cred, string tur);
    void get(string* cod, string* cred, string* tur);
    string getCodigo();

};
#endif

