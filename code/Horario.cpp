#include <iostream>
#include <string>
#include <fstream>
#include "Horario.h"

using namespace std;
Horario::Horario(string cod = " ", string cred = " ", string tur = " ")
{
    set(cod, cred, tur);
}

void Horario::set(string cod, string cred, string tur)
{
    codigo = (cod + ";");
    credito = (cred + ";");
    turma = tur;
}

void Horario::get(string* cod, string* cred, string* tur)
{
    *cod = codigo;
    *cred = credito;
    *tur = turma;
}

string Horario::getCodigo()
{
    return codigo;
}

