#ifndef ALUNO_H
#define ALUNO_H

#include <string>
#include <iomanip>
#include <vector>
#include "Arquivo.h"
#include "Horario.h"

using namespace std;

class Aluno
{
public:

Aluno(string nome, const Arquivo& arqAluno);
void telaInicialDoAluno();
void matricularAlunoEmUmaDisciplina(string cod, string tur, string cred);
void retirarAlunoDeUmaDisciplina(string cod);
void exibirHorarioDoAluno();
string getAlunoNome();
void setNome(string nome);
Arquivo& getArq();

private: string alunoNome;
         Arquivo arquivoAluno;
         vector<Horario> hor;
};
#endif
