/*****************************************************************************************
* Trabalho I da disciplina laboratório de programação II								 *
* Professo: Roland																		 *
* Aluno: Alexandre Carvalho																 *
* Data: 2012/1																			 *
* última revisão: 01/2017																 *
******************************************************************************************
* O programa simula um sistema de matricula, onde o aluno realiza o login através do nro *
* de matricula e então pode realizar a matricula em alguma disciplina.					 *			
* O programa precisa de 3 arquivos .csv com as informações de alunos, disciplinas e      *
* turmas para funcionar como base de dados.												 *
* Através do programa é possível lista os alunos matriculados em uma determinada disci-  *
* plina. Quando um aluno realiza o login é exibido as disciplinas que ele está matricu-  *
* lado.																					 *
*****************************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include "Aluno.h"
#include "Arquivo.h"

using namespace std;

int MenuInicial(); // funcao exibe o menu principal Primeira coisa que aparece no programa
void loginAluno(string*, string*);

int main()
{
    Arquivo arq;
    int opcaoMenu;
    string nomeAluno, matriculaAluno, c, t, h;

    do
    {
        arq.ListaDeChamada(); // Gera os arquivos .tur
        opcaoMenu = MenuInicial(); // Retorna o que o usuário selecionou
        system("clear");
        switch(opcaoMenu)
        {
            // opção 1 do case é onde vc pode imprimir a lista de chamada de algo turma
            // precisa passar o codigo da disciplina e depois a turma.
        case 1:
            cout << "Digite o codigo da disciplina: ";
            cin >> c;
            // depois que o usuario informa o codigo, o sistema faz uma pesquisa e exibe
            // as turmas da disciplina correspondente ao codigo digitado.
            arq.PesquisarTurmas("turmas.csv", c, &h);
            cout << "Digite a turma que voce deseja obter a lista de chamada: ";
            cin >> t;
            system("clear");
            // depois que escolhe a turma então chama o metodo e exibe a turma.
            arq.ExibirListaDeChamada(c, t);
            break;
            // opção 2 do case é onde realiza o login do aluno e as demais operações
            // relacionado ao aluno.
        case 2:
            // função para realizar o login do aluno
            loginAluno(&nomeAluno, &matriculaAluno);
            // Um teste para verificar se foi logado o aluno, se não, foi porque o numero de
            // matricula era invalido então exibe a mensagem, caso deu certo então entra em
            // um loop onde o aluno escolhe as operações a serem feitas.
            if(nomeAluno.length() == 0)
                cout << "matricula invalida" << endl;
            else
            {
                int opcaoAlu;

                Aluno alu(nomeAluno, Arquivo(matriculaAluno));
                do
                {

                    string vazia;
                    // Chama de novo o metodo de Lista de chamada para atualizar as listas, pois
                    // o aluno pode ter se matriculado em uma disciplina ou excluído uma disciplina
                    arq.ListaDeChamada();
                    // O horário do aluno sempre é mostrado quando algum aluno faz o login, e então
                    // é atualizado sempre que fizer alguma modificação.
                    alu.exibirHorarioDoAluno();
                    // Uma tela inicial que exibe as opções do aluno. 1 - matricular em uma disciplina
                    // 2 - excluir uma disciplina e 0 é logout
                    alu.telaInicialDoAluno();
                    cin >> opcaoAlu;
                    // misturei cin com getline e para não ter aquele erro de pular o próximo
                    // getline depois do cin faço ele ler uma string vazia.
                    getline(cin, vazia);

                    if (opcaoAlu == 1)
                    {

                        string arqui("disciplinas.csv");
                        string arquiTurma("turmas.csv");
                        string nome, codigoDisciplina, turma, credito, hora;

                        cout << "Digite o nome da disciplina ou uma palavra relacionada a disciplina: ";
                        getline(cin, nome);
                        while(nome.length() < 5)
                        {
                            cout << "Digite uma palavra com no minimo 5 caracteres" << endl;
                            getline(cin, nome);
                        }
                        // esse metodo exibe na tela as disciplinas que contem o nome que o aluno informou
                        arq.PesquisarDisciplinas(arqui, nome, &credito);
                        cout << "Digite o codigo da disciplina: ";
                        cin >> codigoDisciplina;
                        getline(cin, vazia);
                        while(codigoDisciplina.length() < 5)
                        {
                            cout << "Digite um codigo com no minimo 5 caracteres" << endl;
                            getline(cin, codigoDisciplina);
                        }
                        // Exibe as turmas da disciplina escolhida para se matricular
                        arq.PesquisarTurmas(arquiTurma, codigoDisciplina, &hora);
                        cout << "Escolhe sua turma: ";
                        cin >> turma;
                        system("clear");
                        // Depois de ter escolhido a disciplina e a turma então ela é inserida no horario do aluno
                        alu.matricularAlunoEmUmaDisciplina(codigoDisciplina, turma, credito);
                    }
                    else if (opcaoAlu == 2)
                    {
                        string codigoDisciplina;

                        cout << "Digite o codigo da disciplina que deseja cancelar" << endl;
                        cin >> codigoDisciplina;
                        system("clear");
                        alu.retirarAlunoDeUmaDisciplina(codigoDisciplina);
                    }
                }
                while(opcaoAlu != 0);
            }
            break;
        default:
            cout << "Opcao invalida!" << endl;
        }
    }// fim do do while
    while(opcaoMenu != 0);

    return 0;
}

int MenuInicial() // funcao exibe o menu principal Primeira coisa que aparece no programa
{
    int opcaoMenu;

    do
    {
        cout << "Menu: Digite a opcao desejada:" << endl;
        cout << "\t 1 - Exibir os alunos matriculados em uma disciplina." << endl;
        cout << "\t 2 - Login do Aluno para realizar a matricula de disciplinas." << endl;
        cout << "\t 0 - Finalizar o programa." << endl;
        cin >> opcaoMenu;
        if(opcaoMenu != 1 && opcaoMenu != 2 && opcaoMenu != 0)
            cout << "Digito Invalido. Por favor entre com um novo digito:" << endl;

    }
    while(opcaoMenu != 1 and opcaoMenu != 2 and opcaoMenu != 0);

    return opcaoMenu;
}

// Funcao realiza o login do aluno
void loginAluno(string* refNomeAluno, string* refMatriculaAluno)
{
    string matriculaAluno, nomeAluno;

    cout << "digite o numero de sua matricula" << endl;
    cin >> matriculaAluno;

    ifstream arq;
    arq.open("alunos.csv", ios::in); // Lendo o arquivo
    if (!arq.is_open()) // teste para ver se o arquivo abriu corretamente
    {
        exit (1); // sair do programa
    }

    do
    {
        string matricula, nome;
        getline(arq, matricula, ';');
        getline(arq, nome);

        if (matriculaAluno == matricula)
        {
            *refNomeAluno = nome;
            *refMatriculaAluno = matricula;
        }

    }
    while(arq.good());

    arq.close();
}
