#ifndef ARQUIVO_H
#define ARQUIVO_H
#include <string>
#include <vector>
#include "Horario.h"
using namespace std;

class Arquivo{
string matricula;

public:
    Arquivo(string matriculaAluno = " ");
    void PesquisarDisciplinas(string arq, string palavra, string* credito);
    void PesquisarTurmas(string arq, string codigo, string* horario);
    void DadosParaMemoria(vector<Horario>* hor);
    void GravarHorarioAluno(vector<Horario> *hor);
    bool VerificaConflitoHorario(vector<Horario> *hor, string codNovaDisc, string tur);
    string BuscaHora(string cod, string tur);
    bool VerificaDadosValidos(string cod, string tur);
    string getAlunoMatricula();
    void ListaDeChamada();
    void ExibirListaDeChamada(string cod, string tur);
};
#endif
