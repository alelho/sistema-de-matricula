#include <iostream>
#include <string>
#include <fstream>
#include <list>
#include "Arquivo.h"

using namespace std;

void ObterAlunos(list<string>*, string, string, string, string); // Protótipo da função
void GravarListaDeChamada(list<string>*, string, string);
string RetornaDisciplina(string);
string RetornaMatricula(string);

Arquivo::Arquivo(string matriculaAluno)
{
    matricula = matriculaAluno;
}

void Arquivo::PesquisarDisciplinas(string arq, string palavra, string* credito)
{
    ifstream arqui;
    arqui.open(arq.c_str(), ios::in);

    if (!arqui.is_open())
        cout << "não foi possivel abrir o arquivo" << endl;

    string cabecallho;
    getline(arqui, cabecallho);

    do
    {
        string cod, nomeDisciplina, creditos;
        unsigned posCod, posDisciplina;

        getline(arqui, cod, ';');
        getline(arqui, creditos, ';');
        getline(arqui, nomeDisciplina);

        posCod = cod.find(palavra);
        posDisciplina = nomeDisciplina.find(palavra);
// Quando posCod ou posDisciplina não encontram a palavra a ser pesquisada eles
// recebem um número muito alto não entendi o porque disso, então a solução que achei
// foi que quando eles encontram a palavra eles recebem a posição dela, como nunca passará
// de 100 então eu fiz um if logo abaixo para poder exibir o nome da disciplina
        if(posCod < 100 || posDisciplina < 100)
        {
            cout << cod <<" - " << nomeDisciplina << endl;
            *credito = creditos;
        }
    }
    while(arqui.good());
}

void Arquivo::DadosParaMemoria(vector<Horario>* hor)
{
    ifstream arqui;
    string mat = matricula + (".hor");
   
    arqui.open(mat.c_str(), ios::in);

// Antes de colocar no vector os horarios do aluno, eu limpo ele porque pode conter
// os horarios dos outros alunos.
    hor->clear();

    if (arqui.is_open())
    {
        do
        {
            string cod, creditos, turma;

            getline(arqui, cod, ';');
            getline(arqui, creditos, ';');
            getline(arqui, turma);
            if(turma.length() > 2 )
            {
                Horario hour(cod, creditos, turma);
                hor->push_back(hour);
            }
        }
        while(arqui.good());
    }
    arqui.close();
}

void Arquivo::PesquisarTurmas(string arq, string codigo, string* horario)
{
    ifstream arqui;
    arqui.open(arq.c_str(), ios::in);

    string cabecalho;
    getline(arqui, cabecalho);

    do
    {
        string cod, turma, hora;
        unsigned posCod;

        getline(arqui, cod, ';');
        getline(arqui, turma, ';');
        getline(arqui, hora);

        posCod = cod.find(codigo);
        if(posCod < 100 )
        {
            cout << "Codigo: " << cod << " turma: " << turma << " horario: " << hora << endl;
        }
    }
    while(arqui.good());

    arqui.close();
}

string Arquivo::getAlunoMatricula()
{

    return matricula;
}

void Arquivo::GravarHorarioAluno(vector<Horario> *hor)
{
    string n1, n2, n3;
    ofstream arqSaida;
    string mat = matricula + (".hor");

    arqSaida.open( mat.c_str() , ios::out );
    if (!arqSaida.is_open())
        cout << "Nao foi possivel criar o arquivo" << endl;

    for(vector<Horario>::iterator it = hor->begin(); it != hor->end(); it++)
    {
        Horario hor2 = *it;
    // Busca o codigo, credito e turma da disciplina
        hor2.get(&n1, &n2, &n3);

        arqSaida << (n1) << (n2) << n3 << endl;
        if(arqSaida.fail())
        {
            cout << "Erro fatal!" << endl;
        }
    }
    arqSaida.close();
}

bool Arquivo::VerificaConflitoHorario(vector<Horario> *hor, string codNovaDisc, string tur)
{
    string n1, n2, n3, horaNovaDisci("vazia"), horaJaCadastrada, subs, subs2;
    bool conflito;

    horaNovaDisci = BuscaHora(codNovaDisc, tur);
    for(vector<Horario>::iterator it = hor->begin(); it != hor->end(); it++)
    {
        string sub1, sub2, sub3, sub4, sub5, sub6;
        Horario hor2 = *it;
        hor2.get(&n1, &n2, &n3);
// usei substring pq quando eu mandava n1 ele já ia com ; e então eu não conseguia
// comparar lá no metodo buscaHora
        subs = n1.substr(0, 5);
        subs2 = n3.substr(0, 3);

        horaJaCadastrada = BuscaHora(subs, subs2 );

        sub1 = horaNovaDisci.substr(0, 3);
        if(horaNovaDisci.length() > 3)
            sub2 = horaNovaDisci.substr(3, 3);
        if(horaNovaDisci.length() > 6)
            sub3 = horaNovaDisci.substr(6, 3);

        sub4 = horaJaCadastrada.substr(0, 3);
        if(horaJaCadastrada.length() > 3)
            sub5 = horaJaCadastrada.substr(3, 6);
        if(horaJaCadastrada.length() > 6)
            sub6 = horaJaCadastrada.substr(6, 3);

        if((sub1 == sub4) || (sub1 == sub5) || (sub1 == sub6))
        {
            conflito = true;
            break;
        } 
        else if(sub2.length() > 2)
            if((sub2 == sub4) || (sub2 == sub5) || (sub2 == sub6))
            {
                conflito = true;
                break;
            }
            else
                conflito = false;
        else if (sub3.length() > 2)
            if((sub3 == sub4) || (sub3 == sub5) || (sub3 == sub6))
            {
                conflito = true;
                break;
            }
            else
                conflito = false;
        else
            conflito = false;
    }
  
    return conflito;
}

string Arquivo::BuscaHora(string cod, string tur)
{
    string horaAula;
    ifstream arqui;

    arqui.open("turmas.csv", ios::in);

    string cabecallho;
    getline(arqui, cabecallho);

    do
    {
        string codigo, turma, hora;

        getline(arqui, codigo, ';');
        getline(arqui, turma, ';');
        getline(arqui, hora);

        if(codigo == cod && turma == tur) {
            if(hora[hora.size()-1]=='\r')
                hora = hora.substr(0, hora.size()-1);
            horaAula = hora;

            break;
        }
    }
    while(arqui.good());

    arqui.close();
    return horaAula;
}

bool Arquivo::VerificaDadosValidos(string cod, string tur)
{
    string horaQualquer;

    horaQualquer.clear();

    horaQualquer = BuscaHora(cod, tur);

    if(horaQualquer.length() > 0)
        return true;
    else
        return false;
}

void Arquivo::ListaDeChamada()
{
    list<string> listaAlunos;
    ifstream arqui;
    arqui.open("turmas.csv", ios::in);

    if (!arqui.is_open())
        cout << "não foi possivel abrir o arquivo turmas" << endl;

    string cabecallho;
    getline(arqui, cabecallho);

    do
    {
        string cod, tur, hora;

        getline(arqui, cod, ';');
        getline(arqui, tur, ';');
        getline(arqui, hora);

        ifstream arqui2;
        arqui2.open("alunos.csv", ios::in);
        if (!arqui.is_open())
            cout << "não abriu arquivo aluno" << endl;

        do
        {
            string mat, nome;

            getline(arqui2, mat, ';');
            getline(arqui2, nome);

            ObterAlunos(&listaAlunos, cod, tur, mat, nome);
        }
        while(arqui2.good());
        arqui2.close();

        GravarListaDeChamada(&listaAlunos, tur, cod);
        listaAlunos.clear(); // Limpa a lista para usar de novo no prox. laço
    }
    while(arqui.good());
    arqui.close();
}

void Arquivo::ExibirListaDeChamada(string cod, string tur)
{
    string turma = cod + "-" + tur + ".tur";
    ifstream arq;
    arq.open(turma.c_str(), ios::in);
    if (!arq.is_open())
        cout << "Esta Turma nao existe!" << endl;

    string disciplina, codigo, t;
    getline(arq, disciplina);
    cout << disciplina << endl;
    getline(arq, codigo);
    cout << codigo << endl;
    getline(arq, t);
    cout << t << endl;

    do
    {
        string aluno;

        getline(arq, aluno);
        cout << aluno << endl;
    }while(arq.good());

    arq.close();
}

// Funções
void ObterAlunos(list<string>* lista, string cod, string tur, string mat, string nome)
{
    string matricula = mat + (".hor");
    ifstream arq;
    arq.open(matricula.c_str(), ios::in);

    do
    {
        string codigo("a"), credito, turma("bb");

        getline(arq, codigo, ';');
        getline(arq, credito, ';');
        getline(arq, turma);

        if((codigo == cod)&&(turma == tur))
            lista->push_back(nome);

    }
    while(arq.good());

    arq.close();
}

void GravarListaDeChamada(list<string>* lista, string tur, string cod)
{
    ofstream arqSaida;
// Fazendo as concatenações necessárias
    string codtur = (cod+="-") + tur + (".tur");
    string disciplina = RetornaDisciplina(cod.substr(0, 5));

    if(lista->size() > 0)
    {
        lista->sort(); // Ordenar nomes por ordem alfabetica
        arqSaida.open( codtur.c_str(), ios::out );
        arqSaida << "Disciplina: " << disciplina << endl;
        arqSaida << "Codigo: " << cod.substr(0, 5) << endl;
        arqSaida << "Turma: " << tur << endl << endl;
        for(list<string>::iterator it = lista->begin(); it != lista-> end(); it++)
        {
            string matricula = RetornaMatricula(*it);
            arqSaida << matricula <<" " << *it << endl;
            if(arqSaida.fail())
            {
                cout << "Erro fatal!" << endl;
            }
        }
    }
    arqSaida.close();
}

string RetornaDisciplina(string cod)
{
    string nomeDisciplina;
    ifstream arqui;
    arqui.open("disciplinas.csv", ios::in);

    if (!arqui.is_open())
        cout << "não foi possivel abrir o arquivo" << endl;

    string cabecallho;
    getline(arqui, cabecallho);

    do
    {
        string codigo, disciplina, creditos;

        getline(arqui, codigo, ';');
        getline(arqui, creditos, ';');
        getline(arqui, disciplina);

        if(cod == codigo )
            nomeDisciplina = disciplina;
    }
    while(arqui.good());

    return nomeDisciplina;
}

string RetornaMatricula(string nome)
{
    string mat;
    ifstream arqui;
    arqui.open("alunos.csv", ios::in);

    if (!arqui.is_open())
        cout << "não foi possivel abrir o arquivo" << endl;

    do
    {
        string matricula, alunoNome;

        getline(arqui, matricula, ';');
        getline(arqui, alunoNome);

        if(nome == alunoNome)
            mat = matricula;
    }
    while(arqui.good());
    arqui.close();
    return mat;
}
